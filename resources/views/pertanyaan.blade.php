@extends('layout.master')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Daftar Pertanyaan</h1>
                    <a href="{{ url('pertanyaan/create')}}">Create New</a>
                </div>
                <!-- <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div> -->
            </div>
        </div>
    </div>

	<div class="container">
	<table class="table">
		<thead>
			<tr>
				<td>Pertanyaan</td>
				<td>Isi</td>
				<td>Dibuat</td>
				<td>Diupdate</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			@foreach($pertanyaan as $row)
			<tr>
				<td>{{$row->judul}}</td>
				<td>{{$row->isi}}</td>
				<td>{{$row->tanggal_dibuat}}</td>
				<td>{{$row->tanggal_diupdate}}</td>
				<td>
					<a href="{{ url('pertanyaan/'. $row->id) }}">
						Show
					</a>
					<a href=""> / </a>
					<a href="{{ url('pertanyaan/'. $row->id.'/edit') }}">
						Edit
					</a>
					<form action="{{ url('pertanyaan/'.$row->id) }}" method="post">
						@csrf
					<input type="hidden" name="_method" value="DELETE">
					<button>Delete</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>    
</div>
@endsection
