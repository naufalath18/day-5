@extends('layout.master')

@section('content')
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Buat Pertanyaan</h1>
                </div>
                <!-- <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div> -->
            </div>
        </div>
    </div>

<div class="container">
	<form method="POST" action="{{url('pertanyaan')}}">
		@csrf
		<div class="form-group">
			<label>Judul</label>
			<input type="text" name="judul" class="form-control">
		</div>

		<div class="form-group">
			<label>Isi</label>
			<textarea name="isi" class="form-control"></textarea>
		</div>


		<button class="btn btn-success">Submit</button>
	</form>
</div>
	  
</div>
@endsection