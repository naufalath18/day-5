<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
    	$data['pertanyaan'] = DB::table('pertanyaan')->get();
    	return view('pertanyaan',$data);

    }

    public function create()
    {
    	return view('pertanyaan-form');
    }

    public function store(Request $request)
    {
    	DB::table('pertanyaan')->insert([
    		'judul' => $request->judul,
    		'isi' => $request->isi,
    		'tanggal_dibuat' => \Carbon\Carbon::now(),
    		'tanggal_diupdate' => \Carbon\Carbon::now()
    	]);

    	return redirect('pertanyaan');
    }

    public function show($id)
    {
    	$data['pertanyaan'] = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('pertanyaan-detail', $data);
    }

    public function edit($id)
    {
		$data['pertanyaan'] = DB::table('pertanyaan')->where('id', $id)->first();
		return view('pertanyaan-edit', $data);
    }

    public function update(Request $request, $id)
    {
    	DB::table('pertanyaan')->where('id',$id)->update([
    		'judul' => $request->judul,
    		'isi'	=> $request->isi,
    		'tanggal_diupdate' => \Carbon\Carbon::now()
    	]);
    	return redirect('pertanyaan');
    }
    public function destroy($id)
    {
		DB::table('pertanyaan')->where('id',$id)->delete();
		return redirect('pertanyaan')    ;
    }
}
